#!/usr/bin/python
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import sys
import urllib.request

var =""

def normalize_whitespace(text):
    "Remove redundant whitespace from a string"
    text = text.rstrip('\n')
    return text

class CounterHandler(ContentHandler):
    def __init__ (self):
        self.inContent = 0
        self.theContent = ""
        self.title = ""

    def startElement (self, name, attrs):
        if name == 'item':
            self.inContent = 1
        elif name == 'title':
            self.inContent = 1
        elif name == 'link':
            self.inContent = 1

    def endElement (self, name):
        global var
        if name == 'item':
            pass
        elif name == 'title':
            self.title = self.theContent
        elif name == 'link':
            var += "<h2><a href = " + self.theContent + ">" + self.title + "</a></h2>"
        if self.inContent:
            self.inContent = 0
            self.theContent = ""

    def characters (self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

# --- Main prog
if len(sys.argv)<1:
    print("Usage: python practica.py <document>")
    print(" <document>: file name of the document to parse")
    sys.exit(1)

# Load parser and driver
url = 'http://barrapunto.com/index.rss'
xmlFile= urllib.request.urlopen(url)

JokeParser = make_parser()
JokeHandler = CounterHandler()
JokeParser.setContentHandler(JokeHandler)

# Ready, set, go!
JokeParser.parse(xmlFile)

def main():
    return var

if __name__ == "__main__":
    main()

from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseNotFound
from .models import Page
from django.views.decorators.csrf import csrf_exempt
from . import parser

formulario = """
            <html><body>
            <form action='' method='post'>\
            <p>Name: <input type='text' name='nombre' value = '{var}' size='50'></p>\
            <p>Content: <input type='text' name='content' size='50'></p>\
            <p>\
                <input type='submit' value='Enviar'>\
                <input type='reset' value='Borrar'>\
            </p>\
            </form></body></html>
"""

html_template = "<li><a href='{name}'>{name}</a></li>"

redirect_template = """
        <p><a href='/'><h2> Return to initial page</a></p></h2>
"""

template_titulares = """
    <body style="background-color:yellow;">
       <fieldset>
           <legend><h1>Titulares Barrapunto</h1></legend>
             {titulares}
       </fieldset>
    </body>
"""

def lista(request):
    if request.method == 'GET':
        titulares = parser.main()
        lista = Page.objects.all()
        if len(lista) == 0:
            response = "<h1>There are no items in the list</h1>"
        else:
            response = "<h1>List of names: <ul>"
            for i in lista:
                response += html_template.format(name = i.name)
            response += "</ul></h1>"
        return HttpResponse(response + template_titulares.format(titulares = titulares))
    else:
        return HttpResponse("<h1>Used method " + request.method + " not correct.</h1>")

@csrf_exempt
def showpage(request, name):
    titulares = parser.main()
    if request.method == "POST":
          new_name = request.POST['nombre']
          new_content = request.POST['content']
          try:
              # elimino el que existia
              p = Page.objects.get(name = new_name)
              p.delete()
              # añado el nuevo
              recurso = Page(name=new_name, content = new_content)
              recurso.save()
              return HttpResponse("<h1>It has been updated successfully" + redirect_template + "</h1>")
          except Page.DoesNotExist:
              recurso = Page(name = new_name, content = new_content)
              recurso.save()
              return HttpResponse("<h1>It has been created successfully" + redirect_template + "</h1>")

    if request.method == "GET":
        try:
            item = Page.objects.get(name = name)
            response = HttpResponse("<h1>" + item.content + "</h1><h2>\
            You can update the content of name </h2>\
            " + formulario.format(var = item.name) + redirect_template + template_titulares.format(titulares = titulares))
        except Page.DoesNotExist:
            response = HttpResponseNotFound("<h1>Page " + name + " not found</h1>\
            <h2>You must fill the form to add it.</h2>" + formulario.format(var = name) + template_titulares.format(titulares = titulares))

        return response

    if request.method != "GET" or request.method != "POST":
        return HttpResponseNotFound("<h1>The method used is not valid</h1>")

from django.db import models

class Page(models.Model):
    name = models.CharField(max_length=248)
    content = models.TextField()

"""
makemigrations
migrate
"""
